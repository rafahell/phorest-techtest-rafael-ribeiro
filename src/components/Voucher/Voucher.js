import React, { useState } from 'react'
import axios from 'axios'
import { Username, Password, businessId, branchId } from '../../helpers/Variables'
import Button from '../Button/Button'
import isEmptyInput from '../../helpers/Helpers'
import * as moment from 'moment'

const Voucher = (props) => {
    const {userClientId} = props;
    const [amount, setAmount] = useState('');
    const [errorMessage, SetErrorMessage] = useState('');
    const [isLoading, SetIsLoading] = useState(false);

    //close modal and refresh page
    const closeBtn = () => {
        SetIsLoading(false)
        window.location.reload()
    }

    //post request btn
    const postCreateVoucher = async (param) => {
        if(isEmptyInput(amount))
            SetErrorMessage("amount can't be empty")
        else
            try {
                const token = `${Username}:${Password}`;
                const encodedToken = Buffer.from(token).toString('base64');
                const session_url = `https://api-gateway-dev.phorest.com/third-party-api-server/api/business/${businessId}/voucher`
                const body = {
                    creatingBranchId: branchId,
                    originalBalance: amount,
                    clientId: userClientId,
                    issueDate : moment(),
                    expiryDate : moment().add(1, 'years')
                }
                let config = {
                    method: 'post',
                    url: session_url,
                    headers: { 'Authorization': 'Basic '+ encodedToken },
                    data: body
                }
                const request = await axios(config)
                SetIsLoading(true)
            } catch (error) {
                alert(error)
                SetErrorMessage("Something is wrong with the server. Please try again later.")
            }
    }

    return (
        <div className="flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
            <div className="md:col-span-1">
                <div className="mt-10 sm:mt-0">
                    <div className="md:grid md:grid-cols-3 md:gap-6">
                        <div className="md:col-span-1">
                            <div className="px-4 sm:px-0">
                                <h2 className="text-lg font-medium leading-6 text-gray-900">Phorest Voucher</h2>
                                <p className="mt-1 text-sm text-gray-600">
                                    Type the amount and create a voucher.
                                </p>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2">
                            <form action="#" method="">
                                <div className="shadow overflow-hidden sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-1 gap-6">
                                            <div className="col-span-4 sm:col-span-2">
                                                <label className="block text-sm font-medium text-gray-700">Amount</label>
                                                <input
                                                    data-testid="amount-input"
                                                    onChange={(e) => setAmount(parseFloat(e.target.value).toFixed(2))}
                                                    type="number"
                                                    name="amount"
                                                    id="amount"
                                                    className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                                    placeholder="Please type amount"
                                                    aria-label="amount"
                                                    aria-required="true"/>
                                            </div>
                                            <span className="text-center error">{ errorMessage }</span>
                                        </div>
                                    </div>
                                    <div className="col-span-12 px-4 py-3 bg-gray-50 text-center sm:px-6">
                                        <button onClick={() => postCreateVoucher()} type="button" className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            Create voucher
                                        </button>
                                    </div>
                                </div>
                            </form>
                            {isLoading &&
                            <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
                                <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                                    <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

                                        <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                                            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                                <div className="sm:flex sm:items-start">
                                                    <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-blue-100 sm:mx-0 sm:h-10 sm:w-10">
                                                    <svg className="h-6 w-6 text-blue-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" aria-hidden="true">
							<path d="M17.684,7.925l-5.131-0.67L10.329,2.57c-0.131-0.275-0.527-0.275-0.658,0L7.447,7.255l-5.131,0.67C2.014,7.964,1.892,8.333,2.113,8.54l3.76,3.568L4.924,17.21c-0.056,0.297,0.261,0.525,0.533,0.379L10,15.109l4.543,2.479c0.273,0.153,0.587-0.089,0.533-0.379l-0.949-5.103l3.76-3.568C18.108,8.333,17.986,7.964,17.684,7.925 M13.481,11.723c-0.089,0.083-0.129,0.205-0.105,0.324l0.848,4.547l-4.047-2.208c-0.055-0.03-0.116-0.045-0.176-0.045s-0.122,0.015-0.176,0.045l-4.047,2.208l0.847-4.547c0.023-0.119-0.016-0.241-0.105-0.324L3.162,8.54L7.74,7.941c0.124-0.016,0.229-0.093,0.282-0.203L10,3.568l1.978,4.17c0.053,0.11,0.158,0.187,0.282,0.203l4.578,0.598L13.481,11.723z"></path>
						</svg>
                                                    </div>
                                                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                                        <h3 className="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                                                            Congratulations!
                                                        </h3>
                                                        <div className="mt-2">
                                                        <p className="text-sm text-gray-500">
                                                            Your voucher has been created on {moment().format('L')}.<br/>
                                                            The total amount is: &euro;{amount}
                                                        </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                                <Button handleClick={() => closeBtn()}>Close</Button>
                                            </div>
                                        </div>
                                </div>
                            </div> }
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Voucher