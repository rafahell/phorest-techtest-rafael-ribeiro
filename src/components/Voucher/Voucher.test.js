import { render, screen, fireEvent } from '@testing-library/react';
import Voucher from './Voucher';
import '@testing-library/jest-dom'
import { axe, toHaveNoViolations } from 'jest-axe'

expect.extend(toHaveNoViolations)

describe('Voucher test', () => {
    test('renders heading title', () => {
    render(<Voucher />);
    const headingComponent = screen.getByText(/Phorest Voucher/i);
    expect(headingComponent).toBeInTheDocument();
    })

    test('renders paragraph', () => {
        render(<Voucher />);
        const paragraphText = screen.getByText(/Type the amount and create a voucher./i);
        expect(paragraphText).toBeInTheDocument();
    })

    //run axe
    it('Should have no accessibility violations', async () => {
        const { getByTestId } = render(<Voucher/>)
        const html = getByTestId('amount-input')
        expect(await axe(html)).toHaveNoViolations()
    })
})