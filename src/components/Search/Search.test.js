import { render, screen, fireEvent } from '@testing-library/react';
import Search from './Search';
import '@testing-library/jest-dom'
import { axe, toHaveNoViolations } from 'jest-axe'

expect.extend(toHaveNoViolations)

describe('Input Search', () => {
    it('change value on input change', () =>{
        const onSearchMock = jest.fn();
        const { getByTestId } = render(<Search onChange={onSearchMock()} />)
        const searchInput = getByTestId('custom-element-input')
        const query = 'nirvana'
        expect(searchInput.value).toEqual('')

        searchInput.onChange = onSearchMock
        fireEvent.change(searchInput, { target: { value: query } })
        expect(onSearchMock).toHaveBeenCalled()
    })

    //Run axe
    it('Should have no accessibility violations', async () => {
        const { getByTestId } = render(<Search/>)
        const html = getByTestId('custom-element-input')
        expect(await axe(html)).toHaveNoViolations()
    })
})