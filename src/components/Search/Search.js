import React,  { useState } from 'react'
import axios from 'axios'
import isEmptyInput from '../../helpers/Helpers'
import { Username, Password, businessId } from '../../helpers/Variables'
import Button from '../Button/Button'
import Table from '../Table/Table'
import './Search.css'

const Search = () => {
    const [search, setSearch] = useState('');
    const [errorMessage, SetErrorMessage] = useState('');
    const [displayUser, SetDisplayUser] = useState([]);
    const [isLoading, SetIsLoading] = useState(false);

    //check if email is valid
    const isValidEmail = (email) => {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    }

    //check if phone is valid
    const isValidPhone = (mobile) => {
        if(mobile.match(/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/)){
            SetErrorMessage("")
            return true
        }else{
            SetErrorMessage("Please use a valid mobile number or email address")
            return false
        }
    }

    //search by email or mobile
    const searchBtn = () => {
        SetIsLoading(false)
        SetErrorMessage("")
        if(isEmptyInput(search))
            SetErrorMessage("Search field can't be empty")
        else if (isNaN(search) && isValidEmail(search))
            getData('email=')
        else if (isValidPhone(search))
            getData('phone=')
    }

    //call api to get user
    const getData = async (param) => {
        try {
            const token = `${Username}:${Password}`;
            const encodedToken = Buffer.from(token).toString('base64');
            const session_url = `https://api-gateway-dev.phorest.com/third-party-api-server/api/business/${businessId}/client?${param}${search}`
            let config = {
                method: 'get',
                url: session_url,
                headers: { 'Authorization': 'Basic '+ encodedToken }
            }
            const request = await axios(config)
            SetDisplayUser(request.data._embedded.clients)
            SetIsLoading(true)
        } catch (error) {
            SetErrorMessage("No user found")
        }
    }

    return (
        <div>
            <div className="flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <h1 className="mt-2 text-center text-3xl font-extrabold text-gray-900">
                        Create a Phorest Voucher
                    </h1>
                    <div className="rounded-md shadow-sm -space-y-px">
                        <div>
                            <label className="sr-only">Email address</label>
                            <input onChange={(e) => setSearch(e.target.value)}
                                data-testid="custom-element-input"
                                id="search"
                                name="search"
                                type="text"
                                className="appearance-none rounded-none relative block w-full px-3 py-2 rounded-md border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                                placeholder="Search by mobile number or email address"
                                aria-label="Search by mobile number or email address"
                                aria-required="true"/>
                            <span className="error text-center">{ errorMessage }</span>
                            <Button data-testid="custom-element-button" handleClick={() => searchBtn()}>Search</Button>
                        </div>
                    </div>
                </div>
            </div>
            {isLoading && displayUser.map((user,index) => {
                    return <Table
                        key={index}
                        userName={user.firstName}
                        userLastName={user.lastName}
                        userEmail={user.email}
                        userMob={user.mobile}
                        userClientId={user.clientId} />
                })
            }
            <div className=" bg-gray-50 hidden sm:block" aria-hidden="true">
                <div className="py-5">
                    <div className="border-t border-gray-200"></div>
                </div>
            </div>
        </div>
    )
}

export default Search
