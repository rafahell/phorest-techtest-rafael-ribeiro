import { render, screen, fireEvent } from '@testing-library/react';
import Button from './Button';
import '@testing-library/jest-dom'
import { axe, toHaveNoViolations } from 'jest-axe'

expect.extend(toHaveNoViolations)

describe('Button', () => {
    it('renders correctly', ()=> {
        render(<Button>Click Me</Button>);
        expect(screen.getByTestId('custom-element')).toHaveTextContent('Click Me')
    })

    it('onClick is called when button is clicked', () =>{
        const mockOnClick = jest.fn();
        const { getByTestId } = render(<Button onClick={mockOnClick()} />)
        const clickIndicator = getByTestId('custom-element')
        fireEvent.click(clickIndicator)
        expect(mockOnClick).toHaveBeenCalledTimes(1)
    });

    // Run axe
    it('Should have no accessibility violations', () =>{
        async (Button) => {
        const { container } = render(<Button />)

        const results = await axe(container)
        expect(results).toHaveNoViolations()
        }
    });
});