import React, { useState } from 'react'
import './Table.css'
import Button from '../Button/Button'
import Voucher from '../Voucher/Voucher'

const Table = (props) => {
    const {userName, userLastName, userEmail, userMob, userClientId} = props;
    const [isLoading, SetIsLoading] = useState(false);

    //open voucher form btn
    const openVoucherForm = () => {
        SetIsLoading(true)
    }

    return (
        <div className="flex flex-col bg-grey-50">
            <div className="overflow-x-auto">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table data-testid="table-element" className="min-w-full divide-y divide-gray-200">
                            <thead className="bg-gray-50">
                                <tr>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Name
                                    </th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Email
                                    </th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Mobile
                                    </th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    </th>
                                    <th scope="col" className="relative px-6 py-3">
                                    <span className="sr-only">Edit</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200">
                                <tr>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                    <div className="flex items-center">
                                        <div className="ml-4">
                                            <div className="text-sm font-medium text-gray-900">
                                                {userName} {userLastName}
                                            </div>
                                        </div>
                                    </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        <div className="text-sm text-left font-medium text-gray-500">
                                            {userEmail  == undefined ? 'None' : userEmail}
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        <div className="text-sm text-left font-medium text-gray-500">
                                            {userMob == undefined ? 'None' : userMob}
                                        </div>
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        <Button handleClick={() => openVoucherForm()}>Add</Button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {isLoading &&
                <Voucher userClientId={userClientId} />
            }
        </div>
        )
    }


export default Table
