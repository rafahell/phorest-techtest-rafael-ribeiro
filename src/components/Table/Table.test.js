import { render, screen } from '@testing-library/react';
import Table from './Table';
import '@testing-library/jest-dom'


describe('table component', () => {

    it('renders a table', () => {
        const { getByTestId } = render(<Table />);
        const table = getByTestId('table-element');
        expect(table).toBeInTheDocument();
    });

    it('table has heading ', () => {
        render(<Table/>);
        const name = screen.getByText("Name");
        const email = screen.getByText("Email");
        const mobile = screen.getByText("Mobile");
        expect(name).toBeInTheDocument();
        expect(email).toBeInTheDocument();
        expect(mobile).toBeInTheDocument();
    });

});