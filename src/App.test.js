import { render, screen } from '@testing-library/react';
import App from './App';

test('renders heading title', () => {
  render(<App />);
  const headingEl = screen.getByText(/Create a Phorest Voucher/i);
  expect(headingEl).toBeInTheDocument();
});
