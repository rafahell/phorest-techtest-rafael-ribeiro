import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Search from './components/Search/Search';

function App() {
  return (
    <BrowserRouter>
        <Switch>
        <Route path="/">
          <Search/>
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
