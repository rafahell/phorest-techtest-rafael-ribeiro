# Create Phorest Voucher

Phorest has a platform on which our customers to build their own applications.

Integrate with Phorest API to create vouchers for clients. The user of the app will need to be able to search for a client by their email address or phone number and once found create a voucher.

## Available Scripts

In the project directory, you can run:

### `npm install`

To install all the depencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### Automated Testing

Automated testing has been created using Jest, React Testing Library and jest-axe testing accessibility . In total are 11 tests and 0 failures.
    ```
        Tests: 11 passed, 11 total
    ```
    ```
        Test Suites: 5 passed, 5 total
    ```

## Libraries

- tailwindcss (https://tailwindcss.com/) - A utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup.
- jest-dom (https://testing-library.com/docs/ecosystem-jest-dom/) - is a companion library for Testing Library that provides custom DOM element matchers for Jest
- jest-axe (https://github.com/nickcolley/jest-axe) - Custom Jest matcher for aXe for testing accessibility
- Axios (https://axios-http.com/) - Promise based HTTP client for the browser
- Momentjs (https://momentjs.com/) - Parse, validate, manipulate, and display dates and times in JavaScript.